<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class posts extends Model
{
    use Notifiable;
    protected $fillable = [
        'user_id','title','posts',
    ];


    
    public function user()
    {
        return $this->belongsTo('App\user','foreign_key');
    }
}
