<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Session;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        return redirect('home');
    }
    
    public function index()
    {   
        $posts=\App\posts::all()->reverse();
        return view('home',[ 'posts' => $posts]);
    }
    


    public function store(Request $request)
    {
        $posts= new \App\posts;
        $posts->user_id= Auth::user()->id;
        $posts->title=$request->get('title');
        $posts->posts=$request->get('posts');        
        $posts->save();
        return redirect('home')->with('success', 'Information has been added');
    }


    public function edit($id)
    {   $posts = \App\posts::find($id);
        return view('edit',compact('posts','id'));
    }


    public function update(Request $request, $id)
    {
        $posts= \App\posts::find($id);
        $posts->user_id= Auth::user()->id;
        $posts->title=$request->get('title');
        $posts->posts=$request->get('posts');
        $posts->save();
        return redirect('home')->with('success', 'Information has been added');
    }


    public function destroy($id)
    {
        $posts = \App\posts::find($id);
        $posts->delete();
        return redirect('home')->with('success', 'Information has been deleted');
    }


}
