@extends('layouts.app')

@section('content')

<div class="container">
      <h2>add posts</h2><br/>
      <form method="post" action="{{ url('home')}}" enctype="multipart/form-data">
        @csrf
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="title">title:</label>
            <input type="textarea" class="form-control" name="title">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="posts">Posts: </label>
              <input type="textarea" class="form-control" name="posts">
            </div>
          </div>
          <div class="row">
          <div class="form-group col-md-4" style="margin-top:60px">
            <button type="submit" class="btn btn-success">Submit</button> 
          </div>
        </div>
      </form>
    </div>

     <h2> posts</h2><br/>

        @foreach( $posts as $post )
           <div>
           {{ $post['id']}}
           <div>TITLE=====> {{ $post['title']}}</div>
           <div>
           {{ $post['posts']}}
           </div>
           @if($post['user_id']==Auth::user()->id)  
           <a href="{{ action('HomeController@edit',$post['id'])}}">edit</a>
           <form action="{{ action('HomeController@destroy',$post['id'])}}" method="post">
            @csrf
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
           @endif
           
           <br>
        @endforeach
    </div>

   
@endsection
