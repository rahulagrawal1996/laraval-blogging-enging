@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Edit posts {{ $id }}</h2><br  />
        <form method="post" action="{{ action('HomeController@update',$id) }} " >
            @csrf
            <input name="_method" type="hidden" value="PATCH">
            <div>
                <label for="title">title:</label>
                <input type="text" name="title" value="{{$posts->title}}">
            </div>
            <div>
                <label for="posts">posts:</label>
                <input type="text" name="posts" value="{{$posts->posts}}">
            </div>

            <div class="form-group col-md-4" style="margin-top:60px">
                <button type="submit" class="btn btn-success" style="margin-left:38px">Update</button>
             </div>
      </form>
      </div>

@endsection
